#ifndef HEALTHPACKS_H
#define HEALTHPACKS_H

#include "imagegenerator.h"
#include "config.h"

class HealthPacks : public Tile, public ImageGenerator
{
public:
    HealthPacks(int x, int y, float value);
};

#endif // HEALTHPACKS_H
