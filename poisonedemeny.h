#ifndef POISONEDEMENY_H
#define POISONEDEMENY_H

#include "imagegenerator.h"

class PoisonedEmeny : public PEnemy , public ImageGenerator
{
public:
    PoisonedEmeny(int x,int y,float strength);
    virtual ~PoisonedEmeny();
};

#endif // POISONEDEMENY_H
