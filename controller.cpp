#include "controller.h"
#include "strategy.h"
#include <QtWidgets>
#include <QApplication>
#include <iostream>

Controller * Controller::instance = 0;

/**
 * @brief Controller::Controller : Constructor
 */
Controller::Controller():
    m_numberOfEnemy(0),m_numberOfHealthPacks(0),m_startX(0),
    m_startY(0),m_endX(0),m_endY(0)
{
    m_model = std::make_shared<Model>();
    m_view = std::make_shared<Display>();
    m_currentWorld = m_model->getThe_world();
    m_worldTiles = m_currentWorld->createWorld(TESTWORLD);

    m_pathFinder = std::make_shared<Astar> (m_currentWorld->getRows(),m_currentWorld->getCols());
    m_pathFinder->createNodes(m_worldTiles);
    mW = new MainWindow();
}


/**
 * @brief Controller::~Controller : Destructor
 */
Controller::~Controller()
{
    delete instance;
    delete mW;
}

/**
 * @brief Controller::Create : Singleton approach of creating controllers instance
 */
void Controller::Create()
{
    if(instance != nullptr){
        return;
    }
    instance = new Controller();
}

/**
 * @brief Controller::getControllerInstance : return instance of controller class
 * @return  controller's instance
 */
Controller *Controller::getControllerInstance()
{
    return instance;
}

/**
 * @brief Controller::controllerSignalConnector : Connect UI Widgets SIGNALS to their respective SLOTS
 */
void Controller::controllerSignalConnector(){
    mW->createUI();
    mW->connectSignalsToSlots();
}


/**
 * @brief Controller::initGame : Initialize the game with number of enemy and healthpacks
 * NOTE: If enemy and healthpacks are zero, the default values of 8 and 5 are used respectively.
 */
void Controller::initGame()
{
    if(!m_isGameRunning){
        //if the player just presses start without selecting gaming mode,
        //the default is the auto mode
        if(!mW->getIsGameModeSelected()){
            mW->setAutoGameMode();
        }

        if(m_numberOfEnemy == 0 && m_numberOfHealthPacks == 0){
            std::cout << "Default Value for Enemy and HealthPacks used!" << std::endl;
            this->m_numberOfEnemy  = 8;
            this->m_numberOfHealthPacks = 5;
        }
        /*Create the World and it's enemy*/
        /*NOTE: when enemy is 0, there is a crash as we will try to access 0 sized emeny vector
                in model.cpp -> setOurEnemy(int), So it is better to choose healthpacks and enemy both.*/
        m_model->setOurEnemies(m_numberOfEnemy);
        m_model->setOurHPs(m_numberOfHealthPacks);
        m_model->setOurProtagonist();
        startGame();
        mW->getDisplay().getTerminalButton()->setEnabled(true);
        m_isGameRunning = true;
    }
}

void Controller::startGame()
{
    /*current pixmapitem of the world*/
     m_currentWorldPixMap = &mW->getCurrentPixMapWorld();
     /*current graphics view in use*/
     m_currentGraphicsView = &(std::dynamic_pointer_cast<Display> (m_view)->getView());



     /*------Draw the protagonist------*/
     m_protagonist = std::make_shared<Hero>();
     m_protaPixmap = &m_protagonist->getNewPixMapItem();

     m_protaPixmap->setFlag(QGraphicsItem::ItemIsFocusable);
     m_protaPixmap->setFocus();
     mW->setCurrentProtagonist(*m_protaPixmap);

     mW->setObjectsPositionInView(m_protagonist->getXPos(),m_protagonist->getYPos(),*m_protaPixmap);

    /*-------Draw enemies--------*/
    std::vector<std::shared_ptr<Enemy>> enemyVector;

    enemyVector = m_model->getOurEnemies();

    /*position of objects in the world*/
    int xPos, yPos;
    /*strength of an object*/
    float value;

    /*Distinguish normal and poisened enemy and store them in a container, draw them*/
    for(auto &en : enemyVector){
        xPos = en->getXPos();
        yPos = en->getYPos();
        value = en->getValue();
        /*Dynamic cast to seperate normal and poisened enemy*/
        if((std::dynamic_pointer_cast<PEnemy>(en)) != nullptr){
            std::shared_ptr<PoisonedEmeny> newPoisonedEnemy = std::make_shared<PoisonedEmeny> (xPos,yPos,value);
            m_allEnemies.push_back(newPoisonedEnemy);
            QGraphicsPixmapItem * pixmap = &newPoisonedEnemy->getNewPixMapItem();
            mW->setObjectsPositionInView(xPos,yPos,*pixmap);
        }else{
            std::shared_ptr<NormalEnemy> newNormalEnemy = std::make_shared<NormalEnemy> (xPos,yPos,value);
            m_allEnemies.push_back(newNormalEnemy);
            QGraphicsPixmapItem * pixmap = &newNormalEnemy->getNewPixMapItem();
            mW->setObjectsPositionInView(xPos,yPos,*pixmap);
        }
    }

    /*--------Draw the Health Packs--------*/
    std::vector<std::shared_ptr<Tile>> healthPacksVector;
    healthPacksVector = m_model->getProtagonistHp();

    for(auto &hp : healthPacksVector){
        xPos = hp->getXPos();
        yPos = hp->getYPos();
        value = hp->getValue();

        std::shared_ptr<HealthPacks> newHealthPack = std::make_shared<HealthPacks>(xPos,yPos,value);
        m_allHP.push_back(newHealthPack);
        QGraphicsPixmapItem * pixmap = &newHealthPack->getNewPixMapItem();
        mW->setObjectsPositionInView(xPos,yPos,*pixmap);
    }

    /*------- check auto mode or manual mode----------*/
    //if auto mode then only start the strategy
    if(mW->getDisplay().getAutoModeButton().isChecked()){
        m_strategy = std::make_shared<Strategy>();
        m_strategy->initStrategy();
    }
}

/**
 * @brief Controller::initMazeSolverWorld   : initializing the mazeSolver before starting it.
 * NOTE: this function is executed before startMazeSolver() from MainWindow::setMazeSolverMode().
 * startMazeSolver() only starts after startPathFinding button has been clicked.
 */
void Controller::initMazeSolverWorld(const QString& image)
{
    /*create a new world for pathfinding*/
   // m_model->makeTheWorld(image);
    /*Get the world*/
    m_currentWorld = m_model->getThe_world();
    m_worldTiles = m_currentWorld->createWorld(image);
    m_pathFinder = std::make_shared<Astar>(m_currentWorld->getRows(),m_currentWorld->getCols());

    // if nodes were not created before create them.
    if(!m_areNodesCreated){
       m_allTheNodes = m_pathFinder->createNodes(m_worldTiles);
       m_areNodesCreated = true;
    }
}

/**
 * @brief Controller::startMazeSolver   : start solving the maze
 */
void Controller::startMazeSolver()
{
   /*disconnect the startPathFinding button.
     Only after changing the values of the spinbox of the pathfinder window
     the startPathFinding button can be clicked again!*/
     mW->disConnectSignalsToSlots();

    //reset all the nodes
     m_pathFinder->resetAllTheNodes();

     auto& source_ = m_pathFinder->getANode({m_startX,m_startY});
     auto& target_ = m_pathFinder->getANode({m_endX, m_endY});
     auto &m_path = findPath(source_, target_);
     drawPath(m_path,5); ////5 is the scale or size of teh rectangle
}

/**
 * @brief Controller::restartGame   : restart our entire game application
 */
void Controller::restartGame()
{
    qApp->quit();
    QProcess::startDetached(qApp->arguments()[0], qApp->arguments());
}


void Controller::drawPath(auto &m_path, int scale)
{
    if(m_path.size() > 0){
    mW->removePathFromScene();
    for(auto &p: m_path){
        mW->createAndDrawRect(p.x,p.y,scale);
    }
    }
}

std::vector<Coord>& Controller::findPath(auto& source_, auto& target_)
{
    m_pathFinder->resetAllTheNodes();
    auto &path =  m_pathFinder->aStar(source_, target_);
    m_timeForfindingPath = m_pathFinder->getTimeTakenToFindPath();
    std::reverse(path.begin(),path.end());
    return path;
}

/*--------------------- (x,y) Spinner Slots of the pathFinder UI ---------------*/
void Controller::receiveStartXSpinnerValue(int value){
    m_startX = value;
    mW->ReConnectSignalsToSlots();
}

void Controller::receiveStartYSpinnerValue(int value){
    m_startY = value;
    mW->ReConnectSignalsToSlots();
}

void Controller::receiveEndXSpinnerValue(int value){
    m_endX = value;
    mW->ReConnectSignalsToSlots();
}

void Controller::receiveEndYSpinnerValue(int value){
    m_endY = value;
    mW->ReConnectSignalsToSlots();
}
/*-----------------------------------------------------------------------------*/

/*
int Controller::runInTerminal(){
    std::cout << "Please select one of the following mazes:\n"
                 "for maze 1 type 1\n"
                 "for maze 2 type 2\n"
                 "for maze 3 type 3\n    Controller::runInTerminal(theModel);"
                 "for the world type 4\n"<< std::endl;
    int choice;
    do{
        std::cin >> choice;
        if(choice < 1 || choice > 4){
            std::cout << "Your choice is invalid!\n"
                         "Please select one of the following mazes:\n"
                         "for maze 1 type 1\n"
                         "for maze 2 type 2\n"
                         "for maze 3 type 3\n"
                         "for the world type 4\n"<<std::endl;
        }
    }while(choice < 1 || choice > 4);
    QString path;
    if(choice == 1) path = maze1;
    else if(choice == 2) path = maze2;
    else if(choice == 3) path = maze3;
    else path = world;
    std::cout << "Please select one of the following difficulties:\n"
                 "for easy type 1\n"
                 "for regular type 2\n"
                 "for hard type 3"<< std::endl;
    do{
        std::cin >> choice;
        if(choice < 1 || choice > 3){
            std::cout << "Your choice is invalid!\n"
                         "Please select one of the following difficulties:\n"
                         "for easy type 1\n"
                         "for regular type 2\n"
                         "for hard type 3"<<std::endl;
        }
    }while(choice < 1 || choice > 3);
    std::shared_ptr<Model> theModel = std::make_shared<Model>();
    theModel -> makeTheWorld(path , choice);
    return 0;
}
*/
