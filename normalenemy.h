#ifndef NORMALENEMY_H
#define NORMALENEMY_H

#include "imagegenerator.h"

class NormalEnemy : public Enemy, public ImageGenerator
{
public:
    NormalEnemy(int x,int y, float strength);
    virtual ~NormalEnemy();
};

#endif // NORMALENEMY_H
