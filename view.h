#ifndef VIEW_H
#define VIEW_H
#include <memory>
#include <vector>
#include <QSpinBox>
#include <QToolButton>
#include "./lib/world.h"

class View
{

protected:
    virtual bool userAction() = 0;

    virtual bool makeView() = 0;
    virtual bool moveView(int x, int y) = 0;
};

#endif // VIEW_H
