#ifndef IMAGEGENERATOR_H
#define IMAGEGENERATOR_H

#include <QGraphicsPixmapItem>
#include <QPixmap>
#include <QString>
#include "config.h"

class ImageGenerator
{
public:
    ImageGenerator(const QString &imagePath);
    virtual ~ImageGenerator();
    QGraphicsPixmapItem & getNewPixMapItem();
private:
    QGraphicsPixmapItem * pixmap;
};

#endif // IMAGEGENERATOR_H
