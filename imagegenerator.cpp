#include "imagegenerator.h"
#include <memory>
#include <QDebug>

ImageGenerator::ImageGenerator(const QString &imagePath)
{
    std::shared_ptr<QImage> image = std::make_shared<QImage>(imagePath);
    /*scaled() -> Scaling the image to a rectangle with the given width and height*/
    /*Scale the image's width and height to 1 pixel of the world, where the image is displayed*/
    *image = image->scaled(WORLDSCALE, WORLDSCALE, Qt::KeepAspectRatio);
    pixmap = new QGraphicsPixmapItem(QPixmap::fromImage(*image));
}

ImageGenerator::~ImageGenerator()
{
    delete pixmap;
}

QGraphicsPixmapItem &ImageGenerator::getNewPixMapItem()
{
    return *pixmap;
}
