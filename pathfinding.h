#ifndef PATHFINDING_H
#define PATHFINDING_H

#include "config.h"
#include "node.h"
#include <memory>
#include <QElapsedTimer>

struct Coord{
    int x,y;
    float value=-100; //dummy initializing value
};

/*Sort in ascending order of F cost*/
struct sortNodes
{
        bool operator() (Node* a_, Node* b_)
        {
            float aFscore = a_->getGScore() + a_->getHScore();
            float bFscore = b_->getGScore() + b_->getHScore();
            return (aFscore > bFscore);
        }
};

class Astar: public QObject

{
     Q_OBJECT
public:
    Astar(int rows, int cols);
    float calculateHScore(Node* n_);
    long long int getTimeTakenToFindPath() const;
    std::shared_ptr<Node> &getANode(Coord coordinate_);
    std::vector<Coord> &aStar(std::shared_ptr<Node> &startNode_, std::shared_ptr<Node> &targetNode_);
    std::vector<std::shared_ptr<Node>> &createNodes(const std::vector<std::unique_ptr<Tile>> &tiles_);
    void resetAllTheNodes();

private:
    int m_worldRows;
    int m_worldCols;
    long long int m_timer;
    std::vector<Coord> m_path;
    std::shared_ptr<Node> m_target;
    std::vector<Coord> m_direction;
    std::shared_ptr<Node> m_dummyNode;
    bool detectCollision(Coord coordinates_);
    std::vector<std::shared_ptr<Node>> m_allTheNodes;



};
#endif // PATHFINDING_H
