#ifndef CONTROLLER_H
#define CONTROLLER_H
#include "view.h"
#include "model.h"
#include "display.h"
#include "mainwindow.h"
#include "terminal_ui.h"
#include "poisonedemeny.h"
#include "normalenemy.h"
#include "model.h"
#include "display.h"
#include <memory>
#include "hero.h"
#include "healthpacks.h"
#include "pathfinding.h"
#include "node.h"
#include <QKeyEvent>

class Strategy;
class Controller : public QObject
{
    Q_OBJECT
public:
    /*Singleton*/
    virtual ~Controller();
    static void Create();
    static void Destroy();
    static Controller * getControllerInstance();

    void controllerSignalConnector();

    int getWorldsHeight(){return m_currentWorld->getRows();}
    int getWorldsWidth(){return m_currentWorld->getCols();}
    int getProtagonistXpos(){return m_protagonist->getXPos();}
    int getProtagonistYpos(){return m_protagonist->getYPos();}
    QGraphicsScene &getGraphicsScene(){return mW->getScene();}

    void setProtagonistXpost(int x){m_protagonist->setXPos(x);}
    void setProtagonistYpost(int y){m_protagonist->setYPos(y);}

    std::shared_ptr<World> &getCurrentWorld(){return m_currentWorld;}
    std::vector<std::shared_ptr<Enemy>>& getAllEnemies() {return m_allEnemies;}
    std::vector<std::shared_ptr<Tile>>& getAllHP() {return m_allHP;}
    std::shared_ptr<Hero>& getProtagonist() {return m_protagonist;}

/*---------PATH FINDING MEMBER VARIABLES-----------*/
    std::shared_ptr<Astar> m_pathFinder;
    std::vector<std::shared_ptr<Node>> m_allTheNodes;
    long long int m_timeForfindingPath;
    int m_worldRow;
    int m_worldCol;


    long long getTimeForfindingPath() const{return m_timeForfindingPath;}
    std::shared_ptr<Astar> &getPathFinder(){return m_pathFinder;}
    std::vector<Coord> &findPath(auto& source_, auto& target_);
    void drawPath(auto &m_path, int scale);

    QGraphicsPixmapItem &getProtaPixmap() const {return *m_protaPixmap;}

    MainWindow &getMainWindow() const{return *mW;}

public slots:
    void initGame();
    void receiveEnemySpinnerValue(int value){m_numberOfEnemy = value;}
    void receiveHealthSpinnerValue(int value){m_numberOfHealthPacks = value;}
    void initMazeSolverWorld(const QString &image);
    void startMazeSolver();
    void restartGame();


private:

    Controller();
    static Controller * instance;
    MainWindow *mW;
    //model and view
    std::shared_ptr<Model> m_model;
    std::shared_ptr<View> m_view;
    std::shared_ptr<Display> m_display;
    std::shared_ptr<World> m_currentWorld;

    std::vector<std::unique_ptr<Tile>> m_worldTiles;
    std::vector<std::shared_ptr<Enemy>> m_allEnemies;
    std::vector<std::shared_ptr<Tile>> m_allHP;
    std::shared_ptr<Strategy> m_strategy;
    std::shared_ptr<Hero> m_protagonist;
    QGraphicsPixmapItem * m_protaPixmap;

    QGraphicsPixmapItem  *m_currentWorldPixMap;
    QGraphicsView * m_currentGraphicsView;

    void startGame();//starts in GUI
    bool m_isGameRunning = false;
    int m_numberOfEnemy;
    int m_numberOfHealthPacks;

    void startTerminal();//starts in terminal

/*----------MAZE SOLVER MEMBER VARIABLES--------*/
    int m_startX,m_startY,m_endX,m_endY;
    bool m_areNodesCreated = false;
public slots:
    void receiveStartXSpinnerValue(int value);
    void receiveStartYSpinnerValue(int value);
    void receiveEndXSpinnerValue(int value);
    void receiveEndYSpinnerValue(int value);

};

#endif // CONTROLLER_H
