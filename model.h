#ifndef MODEL_H
#define MODEL_H
#include <memory>
#include "./lib/world.h"

typedef struct {
    uint enemies;
    uint healthPacks;
} parameters;

class Model
{
public:
    Model();
    //create the world
    void makeTheWorld(QString path);

    //getters
    std::vector<std::shared_ptr<Tile> > &getTheWorldTiles();
    std::vector<std::shared_ptr<Enemy> > getOurEnemies();
    std::vector<std::shared_ptr<Tile>>& getProtagonistHp();
    std::shared_ptr<Protagonist> &getOurProtagonist();
    std::shared_ptr<World> &getThe_world();

    //setters
    void setWorldTiles(QString& path);
    void setOurEnemies(int numberOfEnemies);
    void setOurHPs(int numberOfHealthPacks);
    void setOurProtagonist();

private:
    std::shared_ptr<World> the_world;
    std::vector<std::shared_ptr<Tile>> worldTiles;
    std::vector<std::shared_ptr<Enemy>> ourEnemies;
    std::vector<std::shared_ptr<Tile>> ourHPs;
    std::shared_ptr<Protagonist> ourProtagonist;


};

#endif // MODEL_H
