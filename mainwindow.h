#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "display.h"
#include <QMainWindow>
#include <QGraphicsScene>
#include <QSlider>
#include <QSplitter>

class Display;

namespace Ui {
class MainWindow;
}

class MainWindow : public QWidget
{
    Q_OBJECT

public:
     MainWindow(QWidget *parent = 0);
     void createUI();
     QGraphicsScene &getScene() const;
     void connectSignalsToSlots() const;
     void disConnectSignalsToSlots();
     void ReConnectSignalsToSlots();

     QGraphicsPixmapItem & getCurrentPixMapWorld();
     void setObjectsPositionInView(int x, int y, QGraphicsPixmapItem &pixmapItem);
     void setCurrentProtagonist(QGraphicsPixmapItem &protagonist);
     void createAndDrawRect(int x, int y, int scale);
     void removePathFromScene();
     bool getIsGameModeSelected() const{return m_isGameModeSelected;}

     Display &getDisplay() const {return *display;}
     void moveObjectInScene(int x, int y, QGraphicsPixmapItem &pixmapItem);
     void removeEachPathToBeFollowed(int index_);

protected:
     bool eventFilter(QObject *watched, QEvent *event);
public slots:
     void setManualGameMode();
     void setAutoGameMode();
     void setMazeSolverMode();

private:
     Ui::MainWindow *ui;
     QGraphicsScene *scene;
     QSplitter *splitter;
     Display * display;
     QGraphicsPixmapItem * currentQPixMapWorld;
     QGraphicsPixmapItem * mazeSolverQPixMap;
     QGraphicsPixmapItem * m_currentProtagonist;
     std::vector<QGraphicsRectItem*> m_pathRectangles;

     /*Default is auto*/
     bool m_isGameModeSelected = false;
     bool m_isManualMode = true;
     bool m_isSignalDisconnected = true;
     void createScene();
};
#endif // MAINWINDOW_H
