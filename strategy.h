#ifndef STRATEGY_H
#define STRATEGY_H


#include "controller.h"
#include "pathfinding.h"
#include <QTimer>
#include <QGraphicsPixmapItem>

/*store enemy and it's path from protagonist*/
struct NearestEnemy{
    std::shared_ptr<Enemy> enemy_;
    std::vector<Coord> enemy_path_;
};

/*store protagonist status*/
struct {
    std::shared_ptr<Hero> protagonist_;
    QGraphicsPixmapItem * protagonisst_pixmap;
}protagonistStatus;


/*Ascending ordering of the enemy depending on their distance from protagonist*/
struct orderNearByEnemyBySize{
    bool operator ()(NearestEnemy a_, NearestEnemy b_){
        return a_.enemy_path_.size() < b_.enemy_path_.size();
    }
};


class Strategy: public QObject
{
    Q_OBJECT
public:
    Strategy();
    virtual ~Strategy();

public slots:
    void initStrategy();
    void moveProta();
    void updateProgressBar();

private:
    Controller * m_gameController;
    QTimer * m_mainTimer;
    QTimer * m_conditionalTimer;
    QTimer * m_progressBarTimer;
    std::vector<NearestEnemy> m_nearestEnemy; //vector that stores vector of coordinates(i.e. paths)
    std::vector<std::shared_ptr<Enemy>> m_allTheEnemies;
    std::vector<std::shared_ptr<Tile>> m_allTheHp;
    //findNearestHealth();
    void findNearestEnemy();
    void startfindNearestEnemy();
    void attackEnemy();

    size_t m_pathIndex = 0;
    size_t m_enemyIndex = 0;
    bool m_isPathDrawnOnce = false;
    int m_protaHealthChunk = 0;
};

#endif // STRATEGY_H
