#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "display.h"
#include "config.h"
#include "controller.h"
#include <QGraphicsPixmapItem>
#include <QDebug>
#include <algorithm>
#include <iostream>


MainWindow::MainWindow(QWidget *parent) : QWidget(parent)
{
}

void MainWindow::createUI(){
    qApp->installEventFilter(this);//for handeling keyboard action

    /* A splitter lets the user control the size of child widgets by dragging the boundary between them.
     * Any number of widgets may be controlled by a single splitter.*/
    splitter = new QSplitter;

    QSplitter *vSplitter = new QSplitter;
    vSplitter->setOrientation(Qt::Vertical);
    vSplitter->addWidget(splitter);

    //create scene
    createScene();

    display = new Display();
    //add scene to graphicsView
    display->getView().setScene(scene);
    splitter->addWidget(display);

    QGridLayout *layout = new QGridLayout;
    layout->addWidget(vSplitter);
    setLayout(layout);

    setWindowTitle(tr("c++"));
    QWidget::showMaximized();
    setFocusPolicy(Qt::StrongFocus);
}

void MainWindow::createScene()
{
    scene = new QGraphicsScene(this);
    std::shared_ptr<QImage> image = std::make_shared<QImage>(TESTWORLD);
    currentQPixMapWorld = new QGraphicsPixmapItem(QPixmap::fromImage(*image));
    currentQPixMapWorld->setScale(WORLDSCALE);
    scene->addItem(currentQPixMapWorld);
}


QGraphicsScene &MainWindow::getScene() const
{
    return *scene;
}

QGraphicsPixmapItem & MainWindow::getCurrentPixMapWorld(){
    return *currentQPixMapWorld;
}

/**
 * @brief MainWindow::setObjectsPositionInView  : set a pixmap's position in the world by scaling it and then draws it.
 * @param x : x position of pixmap
 * @param y : y position of pixmap
 * @param pixmapItem    : pixmap to be scaled, set it to (x,y) in the world
 * NOTE: each pixel in the world is scaled to a certain value, so the coordinates system of each pixmap
 * to be plotted must also be scaled to that same size. A pixmap is scaled to 1 pixel in the world.
 */
void MainWindow::setObjectsPositionInView(int x, int y, QGraphicsPixmapItem &pixmapItem)
{
    moveObjectInScene(x, y , pixmapItem);
    scene->addItem(&pixmapItem);
}

/**
 * @brief MainWindow::moveObjectInScene : used to set the offset of an item in the world
 * @param x : x position
 * @param y : y position
 * @param pixmapItem : item to be shifted by (x,y)
 */
void MainWindow::moveObjectInScene(int x, int y,QGraphicsPixmapItem &pixmapItem)
{
    QPointF coordinates(x*WORLDSCALE,y*WORLDSCALE);
    pixmapItem.setOffset(coordinates);
}

/**
 * @brief MainWindow::removeEachPathToBeFollowed    : remove a path rectangle with a given index
 * @param index_    : index of path rectangle to be removed
 * NOTE: the rectangle is removed just before the player starts to move
 */
void MainWindow::removeEachPathToBeFollowed(int index_)
{
    if(m_pathRectangles.size() != 0){
    scene->removeItem(m_pathRectangles.at(index_));
    }
}

/**
 * @brief MainWindow::eventFilter : Receivs the keyboard pressed and performs specified action.
 * @param event : action from keyboard
 * @param watched : object what we want to focus on/filter
 * NOTE: multiple events are captured so we need to filter it our and read just the ones from our current scene.
 */
bool MainWindow::eventFilter(QObject *watched, QEvent *event)
{
    if(m_isManualMode){
        auto con = Controller::getControllerInstance();
    if(event->type() == QEvent::KeyPress){
        if(watched == scene){   //only read event from our scene
            QKeyEvent * keyEvent = static_cast<QKeyEvent *> (event);
        if(keyEvent->key() == Qt::Key_Up){
            if(con->getProtagonistYpos() > 0){
            moveObjectInScene(con->getProtagonistXpos(),con->getProtagonistYpos()-1,*m_currentProtagonist);
            con->setProtagonistYpost(con->getProtagonistYpos()-1);
        }
        }else if(keyEvent->key() == Qt::Key_Right){
            if(con->getProtagonistXpos() < (con->getWorldsWidth()-1)){
            moveObjectInScene(con->getProtagonistXpos()+1,con->getProtagonistYpos(),*m_currentProtagonist);
            con->setProtagonistXpost(con->getProtagonistXpos()+1);
            }
        }else if(keyEvent->key() == Qt::Key_Down){
            if(con->getProtagonistYpos() < (con->getWorldsHeight()-1)){// -1 as our grid starts from 0
            moveObjectInScene(con->getProtagonistXpos(),con->getProtagonistYpos()+1,*m_currentProtagonist);
            con->setProtagonistYpost(con->getProtagonistYpos()+1);
            }
        }else if(keyEvent->key() == Qt::Key_Left){
            if(con->getProtagonistXpos() > 0){
            moveObjectInScene(con->getProtagonistXpos()-1,con->getProtagonistYpos(),*m_currentProtagonist);
            con->setProtagonistXpost(con->getProtagonistXpos()-1);
            }
        }
        }
    }
    }
    return QObject::eventFilter(watched, event);
}


void MainWindow::setCurrentProtagonist(QGraphicsPixmapItem &protagonist){
    m_currentProtagonist = &protagonist;
}

void MainWindow::setManualGameMode()
{
    m_isGameModeSelected = true;
    m_isManualMode = true;
    display->getAutoModeButton().setEnabled(false);
}

void MainWindow::setAutoGameMode(){
    m_isGameModeSelected = true;
    m_isManualMode = false; /*i.e. autoMode is activated so manualmode is false*/
    display->getManualModeButton().setEnabled(false);
    display->getAutoModeButton().setChecked(true);
}

/**
 * @brief MainWindow::setMazeSolverMode : show the mazeSolver uiwidgets and hide the pathfindingUIWidget
 */
void MainWindow::setMazeSolverMode()
{
    display->hideOrDisplayStartingUIWidgets(false);
    display->hideOrDisplayMazeSolverUIWidgets(true);

    auto con = Controller::getControllerInstance();
    con->initMazeSolverWorld(MAZE3);
    int WIDTH = con->getWorldsWidth();
    int HEIGHT = con->getWorldsHeight();

    std::shared_ptr<QImage> mazeImage = std::make_shared<QImage>(MAZE3);
    mazeSolverQPixMap = new QGraphicsPixmapItem(QPixmap::fromImage(*mazeImage));
    display->getView().fitInView(QRectF(0,0,WIDTH,HEIGHT),Qt::KeepAspectRatio);

    scene->addItem(mazeSolverQPixMap);
    display->getView().setScene(scene);
}

/**
 * @brief MainWindow::connectSignalsToSlots : Connect required UI Widget's signals to slots in controller class
 */
void MainWindow::connectSignalsToSlots() const
{
    /*------------- STARTING UI CONNECTIONS ------------*/
    /*connect the auto and manual mode radio button*/
    connect(&display->getAutoModeButton(),SIGNAL(clicked(bool)),this,SLOT(setAutoGameMode()));
    connect(&display->getManualModeButton(),SIGNAL(clicked(bool)),this,SLOT(setManualGameMode()));
    connect(&display->getMazeSolverRadioButton(),SIGNAL(clicked(bool)),this,SLOT(setMazeSolverMode()));

    // Connection is done only after gameController has been instantiated first from main
    auto con = Controller::getControllerInstance();
    connect(display->getStartButton(),SIGNAL(clicked()),con,SLOT(initGame()));
    connect(display->getSpinBoxEnemy(),SIGNAL(valueChanged(int)),con,SLOT(receiveEnemySpinnerValue(int)));
    connect(display->getSpinBoxHealth(),SIGNAL(valueChanged(int)),con,SLOT(receiveHealthSpinnerValue(int)));
    connect(display->getRestartButton(),SIGNAL(clicked(bool)),con,SLOT(restartGame()));

    /*-------------- MAZE SOLVER CONNECTIONS -------------*/
    connect(&display->getStartPathFinding(),SIGNAL(clicked(bool)),con,SLOT(startMazeSolver()));
    connect(&display->getStartXSpinBox(),SIGNAL(valueChanged(int)),con,SLOT(receiveStartXSpinnerValue(int)));
    connect(&display->getStartYSpinBox(),SIGNAL(valueChanged(int)),con,SLOT(receiveStartYSpinnerValue(int)));
    connect(&display->getEndXSpinBox(),SIGNAL(valueChanged(int)),con,SLOT(receiveEndXSpinnerValue(int)));
    connect(&display->getEndYSpinBox(),SIGNAL(valueChanged(int)),con,SLOT(receiveEndYSpinnerValue(int)));
    //connect(&display->getGoToStartingUI(),SIGNAL(clicked(bool)),con,SLOT)
}

/**
 * @brief MainWindow::reConnect_disConnectSignalsToSlots    : disconnect signal from startPathFinding button.
 */
void MainWindow::disConnectSignalsToSlots()
{
    if(!m_isSignalDisconnected){
    auto con = Controller::getControllerInstance();
    //disconnect when path finding is active
    disconnect(&display->getStartPathFinding(),SIGNAL(clicked(bool)),con,SLOT(startMazeSolver()));
    m_isSignalDisconnected = true;
    }
}

/**
 * @brief MainWindow::ReConnectSignalsToSlots   : reconnect the signal from startPathFinding button.
 */
void MainWindow::ReConnectSignalsToSlots()
{
    if(m_isSignalDisconnected){
    auto con = Controller::getControllerInstance();
    connect(&display->getStartPathFinding(),SIGNAL(clicked(bool)),con,SLOT(startMazeSolver()));
    m_isSignalDisconnected = false;
    }
}

/**
 * @brief MainWindow::createAndDrawRect : draw rectancles on the coordinates returned from pathfinding
 * @param x : x coordinate of rectangle
 * @param y : y coordinate of rectangle
 */
void MainWindow::createAndDrawRect(int x, int y,int scale)
{
    auto con = Controller::getControllerInstance();
    display->getTotalTimeDisplayer().display((int)con->getTimeForfindingPath());
    int xPos = x;
    int yPos = y;

    /*rescale the rectangles if we are not drawing for maze*/
    if(scale == WORLDSCALE){
        xPos = xPos*scale;
        yPos = yPos*scale;
    }

    QGraphicsRectItem *rect = new QGraphicsRectItem(xPos, yPos, scale, scale);
    QBrush brush(QColor(255, 0, 0, 127));
    QPen border(Qt::transparent);
    rect->setBrush(brush);
    rect->setPen(border);

    m_pathRectangles.push_back(rect);
    scene->addItem(rect);
}


/**
 * @brief MainWindow::removePathFromScene   : remove previous pathFound coordinates form the scene
 */
void MainWindow::removePathFromScene(){
    if(m_pathRectangles.size() > 0){
        for(auto &c : m_pathRectangles){
            scene->removeItem(c);
        }
        m_pathRectangles.clear();
    }
}

