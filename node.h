#ifndef NODE_H
#define NODE_H

#include "../the_maze_runner/lib/world.h"

class Node: public Tile
{
public:
    Node(int x, int y, float weight);
    void setParent(Node* node_) {m_parent = node_;} //POINTS TO REAL NODE *
    Node* getParent(){return m_parent;}

    //getters
    float getHScore(){return m_hScore;}
    float getGScore(){return m_gScore;}
    bool isOpened(){return m_wasOpened;}
    bool isVisited(){return m_wasVisited;}
    bool isWalkable(){return m_isWalkable;}

    //setters
    void setWasOpened(bool b_){m_wasOpened = b_;}
    void setWasVisited(bool b_){m_wasVisited = b_;}
    void setIsWalkable(bool b_){m_isWalkable = b_;}
    void setHScore(float Hscore){m_hScore = Hscore;}
    void setGScore(float Gscore){m_gScore = Gscore;}
    void resetNode();

private:
    Node* m_parent;
    float m_gScore;
    float m_hScore;
    bool m_wasOpened;
    bool m_isWalkable;
    bool m_wasVisited;
};


#endif // NODE_H

