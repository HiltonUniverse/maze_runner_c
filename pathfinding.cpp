#include "pathfinding.h"
#include <queue>
#include <QDebug>

/**
  * @brief operator + : adds up x and y axis of 2 Vec2i coordinates
  * @param left
  * @param right
  * @return Struct Vec2i
  */
Coord operator + (const Coord& left, const Coord& right){
     return {left.x+right.x , left.y + right.y};
}

Astar::Astar(int rows, int cols)
{
    m_dummyNode = std::make_shared<Node>(-10,-10,-10);  //init dummy node
    m_direction = {{ 0, 1 }, { 1, 0 }, { 0, -1 }, { -1, 0 }};     //neighbouring nodes
    m_worldRows = rows;
    m_worldCols = cols;
}

std::vector<std::shared_ptr<Node>> &Astar::createNodes(std::vector<std::unique_ptr<Tile>> const &tiles_)
{
    qDebug("Creating Nodes");
    for(auto &t: tiles_)
    {
        std::shared_ptr<Node> myNode = std::make_shared<Node>(t->getXPos(), t->getYPos(), t->getValue());
        // checking if a node is a wall
        if(std::isinf(t->getValue())){
            myNode->setIsWalkable(false);
        }
       m_allTheNodes.push_back(myNode);
    }
    return m_allTheNodes;
}

void Astar::resetAllTheNodes()
{
    if(m_allTheNodes.size() > 0){
        for(auto &node : m_allTheNodes){
            node->resetNode();
        }
    }
}

long long int Astar::getTimeTakenToFindPath() const
{
    return m_timer;
}

/**
 * @brief Astar::getNode    : return node at a specified location
 * @param x : x coordinate of the node
 * @param y : y coordinate of the node
 * @return  node or Dummy if the node was not found
 */
std::shared_ptr<Node> &Astar::getANode(Coord coordinate_)
{
    try {
        int location = coordinate_.y*m_worldCols + coordinate_.x;
         std::shared_ptr<Node> a = m_allTheNodes.at(location);
        return m_allTheNodes.at(location);
      }
      catch (const std::out_of_range& oor) {
        //if out of range return dymmy node
        return m_dummyNode;
      }
}

/**
 * @brief Astar::calculateHScore   : used to compute H distance
 * @param n_    : currentNode from where the H distance to the target Node is to be calculated
 * @return  The calculated H distance/cost
 */
float Astar::calculateHScore(Node* n_)
{
        Coord delta =  {abs(n_->getXPos() - m_target->getXPos()),  abs(n_->getYPos() - m_target->getYPos())};
        return static_cast<float>(delta.x + delta.y);
}

/**
 * @brief Astar::detectCollision    : detect if a given neighbour is non-existing.
 * @param coordinates_  : coordinate of the neighbour to detect collision
 * @return
 */
bool Astar::detectCollision(Coord coordinates_)
{
        if(coordinates_.x < 0 || coordinates_.x > m_worldRows||
           coordinates_.y < 0 || coordinates_.y > m_worldCols){
            return true;
        }else{
            return false;
        }
}


std::vector<Coord> &Astar::aStar(std::shared_ptr<Node> &startNode_, std::shared_ptr<Node> &targetNode_)
{
   // qDebug("Pathfinder: start x = %d, start Y = %d",startNode_->getXPos(),startNode_->getYPos());
    //qDebug("Pathfinder: end x = %d, end Y = %d",targetNode_->getXPos(),targetNode_->getYPos());

    m_target = targetNode_;

    Node* startNode = startNode_.get();
    Node* targetNode = targetNode_.get();

    Node* currentNode;

    //opel list
    std::priority_queue<Node*, std::vector<Node *>, sortNodes> openQ;

    //push rootNode to openQ
    openQ.push(startNode);
    startNode->setWasOpened(true);

    QElapsedTimer timer;
    timer.start();
    while (!openQ.empty())
    {
        //pop the node with the best/cheapest F cost
        currentNode = openQ.top();

        //If currentNode is the goal node exit
        if(currentNode == targetNode)
            break;

        openQ.pop();
        currentNode->setWasOpened(false);

        // mark the node as visited before
        currentNode->setWasVisited(true);

        // Get all the neighbour coordinates
        for (unsigned int i=0;i<m_direction.size();i++)
        {
                Coord currentCoordinates= {currentNode->getXPos(),currentNode->getYPos()};
                Coord nearbyPoint = currentCoordinates + m_direction[i];

                Node* nearByNode;
                //detect collision
                if(detectCollision(nearbyPoint)) continue;

                    // Get this neighbor coordinates
                    nearByNode = getANode(nearbyPoint).get();

                    //if neighbor is a wall or the node does not exist
                    if(! nearByNode->isWalkable() || (*nearByNode == *m_dummyNode))
                        continue;

                     int newGScore = currentNode->getGScore() + ((i<4)? 10:14);
                    //if the neighbor was not visited prevously
                     if(!nearByNode->isVisited()){
                         if(!nearByNode->isOpened())
                         {
                            nearByNode->setGScore(newGScore);
                            nearByNode->setHScore(calculateHScore(currentNode));
                            nearByNode->setParent(currentNode);
                            if(!nearByNode->isOpened())
                            {
                             openQ.push(nearByNode);
                             nearByNode->setWasOpened(true);
                            }
                          }
                     }else{ // if the neighbor was visited previously, check the newGScore
                         if(newGScore < nearByNode->getGScore()){
                             nearByNode->setGScore(newGScore);
                             nearByNode->setParent(currentNode);
                         }
                     }
                }
        }

    m_timer = timer.elapsed();
    m_path.clear();
    // store the best path's coordinates
    while (currentNode->getParent() != nullptr)
    {
        m_path.push_back({currentNode->getXPos(), currentNode->getYPos(), currentNode->getValue()});
        currentNode = currentNode->getParent();
    }

    startNode = nullptr;
    targetNode = nullptr;
    currentNode = nullptr;

   // qDebug("path size is: %d",m_path.size());
    return m_path;
}
