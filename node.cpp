#include "node.h"

Node::Node(int x, int y, float weight):Tile{x, y, weight}
{
    m_isWalkable = true;
    resetNode();
}

void Node::resetNode()
{
    m_gScore = 0.0f;
    m_hScore = 0.0f;
    m_parent = nullptr;
    m_wasOpened = false;
    m_wasVisited = false;
}
