#include "model.h"
#include <memory>
#include <vector>
#include "./lib/world.h"
#include <iostream>

Model::Model()
{
    the_world = std::make_shared<World>();
}


void Model::makeTheWorld(QString path){
    setWorldTiles(path);
}

std::vector<std::shared_ptr<Tile>>& Model::getTheWorldTiles()
{
    return worldTiles;
}

std::vector<std::shared_ptr<Enemy>> Model::getOurEnemies()
{
    return ourEnemies;
}

std::vector<std::shared_ptr<Tile>>& Model::getProtagonistHp()
{
    return ourHPs;
}

std::shared_ptr<Protagonist> &Model::getOurProtagonist()
{
    return ourProtagonist;
}

std::shared_ptr<World> &Model::getThe_world()
{
    return the_world;
}

/*-----PRIVATE SETTERS TO SET THE WORLDS BASIC COLLECTIONS AND OBJECTS-----*/
/*---------ALSO CONVERSION OF UNIQUE POINTERS TO SHARED POINTERS-----------*/
void Model::setOurHPs(int numberOfHealthPacks)
{
    /*get the healthpacks*/
    std::vector<std::unique_ptr<Tile>> tempHP = the_world->getHealthPacks(numberOfHealthPacks);
    /*convert them to shared_ptr to change their access mode*/
    for(size_t i=0;i<tempHP.size();i++){
        std::shared_ptr<Tile> t = std::move(tempHP.at(i));
        ourHPs.push_back(t);
    }
}

void Model::setOurProtagonist()
{
    std::unique_ptr<Protagonist> tempProta = the_world->getProtagonist();
    ourProtagonist = std::move(tempProta);
}

void Model::setOurEnemies(int numberOfEnemies)
{
    std::vector<std::unique_ptr<Enemy>> tempEnemy = the_world->getEnemies(numberOfEnemies);
    for(size_t i=0;i<tempEnemy.size();i++){
        std::shared_ptr<Enemy> t = std::move(tempEnemy.at(i));
        ourEnemies.push_back(t);
    }
}

void Model::setWorldTiles(QString& path)
{
    std::vector<std::unique_ptr<Tile>> tempTiles =the_world->createWorld(path);
    for(size_t i=0;i<tempTiles.size();i++){
        std::shared_ptr<Tile> t = std::move(tempTiles.at(i));
        worldTiles.push_back(t);
    }
}


