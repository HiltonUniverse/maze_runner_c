#include "strategy.h"
#include <QDebug>
#include <iostream>

Strategy::Strategy()
{
    m_gameController = Controller::getControllerInstance();

    m_allTheEnemies = m_gameController->getAllEnemies();

    m_allTheHp = m_gameController->getAllHP();

    for(auto &hp: m_allTheHp){
        m_gameController->getPathFinder()->getANode({hp->getXPos(),hp->getYPos()})->setIsWalkable(false);
    }

    protagonistStatus.protagonist_ = m_gameController->getProtagonist();
    protagonistStatus.protagonisst_pixmap = &m_gameController->getProtaPixmap();

    findNearestEnemy(); //find nearest enemy

    m_mainTimer = new QTimer(this);
    connect(m_mainTimer,SIGNAL(timeout()),this,SLOT(moveProta()));

    m_conditionalTimer = new QTimer(this);
    connect(m_conditionalTimer,SIGNAL(timeout()),this,SLOT(initStrategy()));

    m_progressBarTimer = new QTimer(this);
    connect(m_progressBarTimer,SIGNAL(timeout()),this,SLOT(updateProgressBar()));

    m_conditionalTimer->start(CONDITIONAL_TIMER);
}

Strategy::~Strategy(){
    delete m_mainTimer;
}

/*NOTE: MAIN LOGIC OF THE GAME IS HERE*/
void Strategy::initStrategy()
{
    while(protagonistStatus.protagonist_->getHealth() != 0 && protagonistStatus.protagonist_->getEnergy() != 0 && m_nearestEnemy.size() != 0 && m_conditionalTimer->isActive()){
        for(auto &en : m_nearestEnemy){
            if(!(en.enemy_->getDefeated())){
                if(protagonistStatus.protagonist_->getHealth() > en.enemy_->getValue()){
                    m_conditionalTimer->stop();
                    m_mainTimer->start(MAIN_TIMER);
                }
            }
        }
     }
}

/**
 * @brief Strategy::moveProta   : update protagonist's position every time the conditional timer stops and main timer starts.
 */
void Strategy::moveProta(){
    if(m_nearestEnemy.size() != 0 && protagonistStatus.protagonist_->getHealth() > 0){
    if(protagonistStatus.protagonist_->getHealth() > m_nearestEnemy.at(0).enemy_->getValue()){
        qDebug("next enemy health: %f", m_nearestEnemy.at(0).enemy_->getValue());
    //select the fist enemy
    std::vector<Coord> pathToFollow = m_nearestEnemy.at(0).enemy_path_;

    //draw the path to the enemy
    if(!m_isPathDrawnOnce){
        m_gameController->drawPath(pathToFollow,WORLDSCALE);
        m_isPathDrawnOnce = true;
    }

    //move protagonist
    if(pathToFollow.size() > m_pathIndex){
    m_gameController->getMainWindow().moveObjectInScene(pathToFollow.at(m_pathIndex).x,
                                                        pathToFollow.at(m_pathIndex).y,
                                                        *protagonistStatus.protagonisst_pixmap);
    }
    //next position of the protagonist
    m_pathIndex++;

    //if the final coordinate of the path has been reached
    if(pathToFollow.size() < m_pathIndex){
        m_progressBarTimer->start(1);
        protagonistStatus.protagonist_->setPos(pathToFollow.back().x,pathToFollow.back().y);  //new position of the protagonist is the last enemy it killed
        m_nearestEnemy.at(m_enemyIndex).enemy_->setDefeated(true);
        //qDebug("Current Enemy Health: %f ",m_nearestEnemy.at(m_enemyIndex).enemy_->getValue());
        m_gameController->getPathFinder()->getANode({pathToFollow.back().x,pathToFollow.back().y})->setIsWalkable(false);
        m_pathIndex = 0;
        findNearestEnemy();
        m_isPathDrawnOnce = false;
        m_conditionalTimer->start(CONDITIONAL_TIMER);
    }
    }else{//still need some improvement on what to do if the enemies can't be defeated
        m_mainTimer->stop();
        m_conditionalTimer->stop();
        m_progressBarTimer->stop();
        qDebug("Game Over");
        qDebug("protagonist health= %f ",protagonistStatus.protagonist_->getHealth());
    }
    }
}

void Strategy::updateProgressBar()
{
    qDebug("health updated");
    float currentProtaHealth = protagonistStatus.protagonist_->getHealth();
    float currentEnemyHealth = m_nearestEnemy.at(0).enemy_->getValue();
    qDebug("Current Enemy Health: %f ", currentEnemyHealth);
    float newHealth = currentProtaHealth - currentEnemyHealth;
    m_gameController->getMainWindow().getDisplay().getProtaHealth().setValue(newHealth);
    protagonistStatus.protagonist_->setHealth(newHealth);
    qDebug("new Health is: %f", newHealth);
    m_progressBarTimer->stop();
}

void Strategy::findNearestEnemy()
{
    m_nearestEnemy.clear();
    for(auto &e: m_allTheEnemies){
        if(!e->getDefeated()){
            auto& source_ = m_gameController->getPathFinder()->getANode({protagonistStatus.protagonist_->getXPos(),protagonistStatus.protagonist_->getYPos()});
            auto& target_ = m_gameController->getPathFinder()->getANode({e->getXPos(),e->getYPos()});
            auto &m_path = m_gameController->findPath(source_,target_);
            m_nearestEnemy.push_back({e,m_path});
        }
    }

    //sort the enemy in ascending order of distance from protagonist
    std::sort(m_nearestEnemy.begin(),m_nearestEnemy.end(),orderNearByEnemyBySize());
}


/*Testing the ordered enemy and their locations*/
/*
    for(auto &e : m_nearestEnemy){
        //std::cout << "size of ordered path is: " << e.path_.size() << std::endl;
        std::cout << "X: " << e.enemy_->getXPos()<< " ,Y:" << e.enemy_->getYPos() << " size: "
                  << e.enemy_path_.size() << " enemy health: " << e.enemy_->getValue() <<std::endl;

    }
*/
