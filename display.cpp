/*
 * This class is used to Visualize all the widgets such as GraphicsView/Scene, Buttons, zoomIn/Out sliders.
 */
#include "display.h"
#include <QWidget>
#include <iostream>
#include <qstyle.h>
#include <QVBoxLayout>
#include <qmath.h>
#include <QScrollBar>
#include <QGraphicsScene>
#include <QGraphicsPixmapItem>
#include "controller.h"

Display::Display(QWidget *parent):
    QWidget(parent)
{
    Display::makeView();
}

bool Display::makeView()
{
    m_graphicsView = new QGraphicsView(this);
    m_graphicsView->setDragMode(QGraphicsView::ScrollHandDrag); //allows drag and move

    int size =24;   //size of zoomin and zoomout icons: 24 standard size
    QSize iconSize(size, size);

    //zoomInImage
    QToolButton *zoomInIcon = new QToolButton;
    zoomInIcon->setAutoRepeat(true);
    zoomInIcon->setAutoRepeatInterval(33);
    zoomInIcon->setAutoRepeatDelay(0);
    zoomInIcon->setIcon(QPixmap(ZOOMIN));
    zoomInIcon->setIconSize(iconSize);

    //zoomOutImage
    QToolButton *zoomOutIcon = new QToolButton;
    zoomOutIcon->setAutoRepeat(true);
    zoomOutIcon->setAutoRepeatInterval(33);
    zoomOutIcon->setAutoRepeatDelay(0);
    zoomOutIcon->setIcon(QPixmap(ZOOMOUT));
    zoomOutIcon->setIconSize(iconSize);

    //set up slider
    m_zoomSlider = new QSlider;
    m_zoomSlider->setMinimum(0);
    m_zoomSlider->setMaximum(550);
    m_zoomSlider->setValue(250);
    m_zoomSlider->setTickPosition(QSlider::TicksRight);

    // Zoom slider layout
    QVBoxLayout *zoomSliderLayout = new QVBoxLayout;
    zoomSliderLayout->addWidget(zoomInIcon);
    zoomSliderLayout->addWidget(m_zoomSlider);
    zoomSliderLayout->addWidget(zoomOutIcon);

    m_resetButton = new QToolButton();
    m_resetButton->setText("Reset");
    m_resetButton->setEnabled(false);

    QFont f( "Arial", 8, QFont::Bold);
    m_labelProtaHealth = new QLabel("ENERGY:");
    m_labelProtaHealth->setFont(f);
    m_labelProtaHealth->setStyleSheet("QLabel { background-color : pink}");

    m_labelprotaEnergy = new QLabel("HEALTH:");
    m_labelprotaEnergy->setFont(f);
    m_labelprotaEnergy->setStyleSheet("QLabel { background-color : pink}");


    QVBoxLayout * progressBarLayout = new QVBoxLayout;
    progressBarLayout->addWidget(m_labelProtaHealth,0,Qt::AlignCenter);
    progressBarLayout->addWidget(m_labelprotaEnergy,0,Qt::AlignCenter);

    m_labelEnemy = new QLabel("ENEMY AMOUNT:");
    m_labelEnemy->setFont(f);
    m_labelHealth = new QLabel("HEALTHPACK AMOUNT:");
    m_labelHealth->setFont(f);

    //for number of enemy
    m_spinBoxEnemy = new QSpinBox;
    m_spinBoxEnemy->setRange(0,999);
    m_spinBoxEnemy->setSingleStep(1);
    m_spinBoxEnemy->setValue(0);

    //for number of healthpack
    m_spinBoxHealth = new QSpinBox;
    m_spinBoxHealth->setRange(0,999);
    m_spinBoxHealth->setSingleStep(1);
    m_spinBoxHealth->setValue(0);

    QVBoxLayout *enemyHealthLayout = new QVBoxLayout;
    enemyHealthLayout->addWidget(m_labelEnemy);
    enemyHealthLayout->addWidget(m_spinBoxEnemy);
    enemyHealthLayout->addWidget(m_labelHealth);
    enemyHealthLayout->addWidget(m_spinBoxHealth);


    //progressbar for health of protagonist
    m_protaHealth = new QProgressBar;
    m_protaHealth->setRange(0,100);
    m_protaHealth->setValue(100);

    //progressbar for energy of protagonist
    m_protaEnergy = new QProgressBar;
    m_protaEnergy->setRange(0,100);
    m_protaEnergy->setValue(100);

    QVBoxLayout *protaHELayout = new QVBoxLayout;   //layout for progressbar of protagonist
    protaHELayout->addWidget(m_protaEnergy);
    protaHELayout->addWidget(m_protaHealth);

    /*Start & Restart Button*/
    m_startButton = new QToolButton;
    m_startButton->setText("    Start    ");
    m_startButton->setFont(f);
    m_restartButton = new QToolButton;
    m_restartButton->setText("  Restart  ");
    m_restartButton->setFont(f);

    m_terminalButton = new QToolButton();
    m_terminalButton->setText("Terminal  ");
    m_terminalButton->setFont(f);
    m_terminalButton->setEnabled(false);

    QVBoxLayout *re_startButtonLayout = new QVBoxLayout;
    re_startButtonLayout->addWidget(m_startButton);
    re_startButtonLayout->addWidget(m_restartButton);
    re_startButtonLayout->addWidget(m_terminalButton);

    m_manualModeRadioButton = new QRadioButton;
    m_manualModeRadioButton->setText("Manual");
    m_manualModeRadioButton->setFont(f);

    m_autoModeRadioButton = new QRadioButton;
    m_autoModeRadioButton->setText("Auto");
    m_autoModeRadioButton->setFont(f);

    m_mazeSolverRadioButton = new QRadioButton;
    m_mazeSolverRadioButton->setText("MazeSolver");
    m_mazeSolverRadioButton->setFont(f);

    m_modeLabel = new QLabel;
    m_modeLabel->setText("GAME MODE");
    m_modeLabel->setFont(f);
    m_modeLabel->setStyleSheet("QLabel { background-color : pink}");

    QVBoxLayout * modeLayout = new QVBoxLayout;
    modeLayout->addWidget(m_modeLabel,0,Qt::AlignCenter);
    modeLayout->addWidget(m_manualModeRadioButton,0,Qt::AlignTop);
    modeLayout->addWidget(m_autoModeRadioButton,0,Qt::AlignTop);
    modeLayout->addWidget(m_mazeSolverRadioButton,0,Qt::AlignTop);


    QHBoxLayout *enemyProtaLayout = new QHBoxLayout;    //final layout
    enemyProtaLayout->addLayout(enemyHealthLayout);
    enemyProtaLayout->addLayout(progressBarLayout);
    enemyProtaLayout->addLayout(protaHELayout);
    enemyProtaLayout->addLayout(re_startButtonLayout);
    enemyProtaLayout->addLayout(modeLayout);

    //setUp Final layout
    topLayout = new QGridLayout;
    topLayout->addWidget(m_graphicsView, 1, 0);
    topLayout->addLayout(zoomSliderLayout, 1, 1);
    topLayout->addWidget(m_resetButton,2,1);
    topLayout->addLayout(enemyProtaLayout,0,0);

    /*---------ADD ALSO THE MAZE SOLVER UI WIDGETS-----------*/
    drawMazeSolverUI();
    /*Hide the mazeSolverUI Initially*/
    hideOrDisplayMazeSolverUIWidgets(false);
    setLayout(topLayout);

    /*Used for actual zoomin*/
    connect(m_zoomSlider, SIGNAL(valueChanged(int)), this, SLOT(setupMatrix()));
    /*connects the zoomIn/Oout button with the slider's motion*/
    connect(zoomInIcon, SIGNAL(clicked()), this, SLOT(zoomIn()));
    connect(zoomOutIcon, SIGNAL(clicked()), this, SLOT(zoomOut()));

    /*Enable reset button after the user start scrolling*/
    connect(m_graphicsView->horizontalScrollBar(),SIGNAL(valueChanged(int)),this,SLOT(setResetButtonEnabled()));
    connect(m_graphicsView->verticalScrollBar(),SIGNAL(valueChanged(int)),this,SLOT(setResetButtonEnabled()));

    /*when resetButton is clicked(bool),execute resetView()*/
    connect(m_resetButton,SIGNAL(clicked(bool)),this,SLOT(resetView()));

    return false;
}

/**
 * @brief Display::drawMazeSolverUI : Draws the entire maze solver UI
 */
void Display::drawMazeSolverUI()
{
    m_startXLabel = new QLabel;
    m_startXLabel->setText("Start X");

    m_startYLabel = new QLabel;
    m_startYLabel->setText("Start Y");

    QVBoxLayout * startLayout = new QVBoxLayout;
    startLayout->addWidget(m_startXLabel,0,Qt::AlignRight);
    startLayout->addWidget(m_startYLabel,0,Qt::AlignRight);

    m_startXSpinBox = new QSpinBox;
    m_startXSpinBox->setMaximum(100);
    m_startXSpinBox->setMinimum(0);

    m_startYSpinBox = new QSpinBox;
    m_startYSpinBox->setMaximum(100);
    m_startYSpinBox->setMinimum(0);

    QVBoxLayout * startSpinnerLayout = new QVBoxLayout;
    startSpinnerLayout->addWidget(m_startXSpinBox);
    startSpinnerLayout->addWidget(m_startYSpinBox);

    m_endXLabel = new QLabel;
    m_endXLabel->setText("End X");

    m_endYLabel = new QLabel;
    m_endYLabel->setText("End Y");

    QVBoxLayout * endLabelLayout = new QVBoxLayout;
    endLabelLayout->addWidget(m_endXLabel,0,Qt::AlignRight);
    endLabelLayout->addWidget(m_endYLabel,0,Qt::AlignRight);

    m_endXSpinBox = new QSpinBox;
    m_endXSpinBox->setMaximum(2500);
    m_endXSpinBox->setMinimum(0);

    m_endYSpinBox = new QSpinBox;
    m_endYSpinBox->setMaximum(2500);
    m_endYSpinBox->setMinimum(0);

    QVBoxLayout * endSpinnerayout = new QVBoxLayout;
    endSpinnerayout->addWidget(m_endXSpinBox);
    endSpinnerayout->addWidget(m_endYSpinBox);

    m_totalTimeLabel = new QLabel;
    m_totalTimeLabel->setText("Time Taken");

    m_totalTimeDisplayer = new QLCDNumber();

    QVBoxLayout * timerLayout = new QVBoxLayout;
    timerLayout->addWidget(m_totalTimeLabel,0,Qt::AlignHCenter);
    timerLayout->addWidget(m_totalTimeDisplayer,0,Qt::AlignHCenter);

    m_goToStartingUI = new QToolButton();
    m_goToStartingUI->setText  ("GO BACK");

    m_startPathFinding = new QToolButton();
    m_startPathFinding->setText("   START   ");

    QVBoxLayout * mazeSolverButtonLayout = new QVBoxLayout;
    mazeSolverButtonLayout->addWidget(m_startPathFinding,0,Qt::AlignLeft);
    mazeSolverButtonLayout->addWidget(m_goToStartingUI,0,Qt::AlignLeft);


    QHBoxLayout * mazeRunnerLayout = new QHBoxLayout;
    mazeRunnerLayout->addLayout(startLayout);
    mazeRunnerLayout->addLayout(startSpinnerLayout);
    mazeRunnerLayout->addLayout(endLabelLayout);
    mazeRunnerLayout->addLayout(endSpinnerayout);
    mazeRunnerLayout->addLayout(timerLayout);
    mazeRunnerLayout->addLayout(mazeSolverButtonLayout);

    QGridLayout * mazeRunnerFinalLayout = new QGridLayout;
    mazeRunnerFinalLayout->addLayout(mazeRunnerLayout,0,0);

    topLayout->addLayout(mazeRunnerFinalLayout,0,0);
}

/**
 * @brief Display::hidePathFindingUIWidgets : Hide/show the widgets of initial UI
 */
void Display::hideOrDisplayStartingUIWidgets(bool value)
{
   m_protaHealth->setVisible(value);
   m_protaHealth->setVisible(value);

   m_spinBoxEnemy->setVisible(value);
   m_spinBoxHealth->setVisible(value);

   m_labelHealth->setVisible(value);
   m_labelEnemy->setVisible(value);
   m_labelProtaHealth->setVisible(value);
   m_labelprotaEnergy->setVisible(value);

   m_startButton->setVisible(value);
   m_terminalButton->setVisible(value);
   m_restartButton->setVisible(value);
   m_manualModeRadioButton->setVisible(value);
   m_autoModeRadioButton->setVisible(value);
   m_mazeSolverRadioButton->setVisible(value);
   m_modeLabel->setVisible(value);
}

/**
 * @brief Display::hideMazeSolverUIWidgets  : Hide/show the widgets of mazeSolver UI
 */
void Display::hideOrDisplayMazeSolverUIWidgets(bool value)
{
   m_startXLabel->setVisible(value);
   m_startYLabel->setVisible(value);
   m_startXSpinBox->setVisible(value);
   m_startYSpinBox->setVisible(value);

   m_endXLabel->setVisible(value);
   m_endYLabel->setVisible(value);
   m_endXSpinBox->setVisible(value);
   m_endYSpinBox->setVisible(value);

   m_totalTimeLabel->setVisible(value);
   m_totalTimeDisplayer->setVisible(value);
   m_goToStartingUI->setVisible(value);
   m_startPathFinding->setVisible(value);
}

QGraphicsView &Display::getView() const
{
    return *m_graphicsView;
}

/*Just used to move the zoom in slider up or down
  It is not responsible for actual image zoomIn*/
void Display::zoomIn(int level)
{
    m_zoomSlider->setValue(m_zoomSlider->value() + level);

}
/*Just used to move the slider up or down
  It is not responsible for actual image zoomOut*/
void Display::zoomOut(int level)
{
    m_zoomSlider->setValue(m_zoomSlider->value() - level);
}

/*Actually responsible for zooming in the graphics View matrix*/
void Display::setupMatrix()
{
    qreal scale = qPow(qreal(2), (m_zoomSlider->value() - 250) / qreal(50));

    QMatrix matrix;
    matrix.scale(scale, scale);
    m_graphicsView->setMatrix(matrix);
    m_graphicsView->update();
}

/*Enable the reset button*/
void Display::setResetButtonEnabled()
{
    m_resetButton->setEnabled(true);
}

/*Resets the slider's value*/
void Display::resetView()
{
    m_zoomSlider->setValue(220);
    m_resetButton->setEnabled(false);
}

                                    /*Change the return types*/
bool Display::userAction()
{
 return false;
}

bool Display::moveView(int x, int y)
{
    return false;
}


/*GETTERS FOR MAZE SOLVER SPINNER (X,Y) VALUES*/
QSpinBox &Display::getEndYSpinBox() const
{
    return *m_endYSpinBox;
}

QSpinBox &Display::getEndXSpinBox() const
{
    return *m_endXSpinBox;
}

QSpinBox &Display::getStartYSpinBox() const
{
    return *m_startYSpinBox;
}

QSpinBox &Display::getStartXSpinBox() const
{
    return *m_startXSpinBox;
}
/*----------------------------------------------*/

QToolButton &Display::getStartPathFinding() const
{
    return *m_startPathFinding;
}

QToolButton &Display::getGoToStartingUI() const
{
    return *m_goToStartingUI;
}


/**
 * @brief Display::getMazeSolverRadioButton : returns radio button to switch to mazeFinder UI
 * @return
 */
QRadioButton &Display::getMazeSolverRadioButton() const
{
    return *m_mazeSolverRadioButton;
}

QRadioButton &Display::getAutoModeButton() const
{
    return *m_autoModeRadioButton;
}

QRadioButton &Display::getManualModeButton() const
{
    return *m_manualModeRadioButton;
}


// Amir
/*Button to switch from UI to Terminal mode*/
QToolButton *Display::getTerminalButton() const
{
    return m_terminalButton;
}

/*Button that is used to re_start the game*/
QToolButton *Display::getRestartButton() const
{
    return m_restartButton;
}
/*Button that is used to start the game*/
QToolButton *Display::getStartButton() const
{
    return m_startButton;
}

QSpinBox * Display::getSpinBoxHealth() const
{
    return m_spinBoxHealth;
}

QSpinBox * Display::getSpinBoxEnemy() const
{
    return m_spinBoxEnemy;
}
