#ifndef TERMINAL_UI_H
#define TERMINAL_UI_H

#include <iostream>
#include <string>
#include <vector>
#include <memory>
#include "./lib/world.h"
#include "view.h"
#include "model.h"

class Terminal_UI : public View
{
public:
    Terminal_UI(Model&);
    bool userAction();
    bool makeView();
    bool moveView(int x, int y);
private:
    const std::string aliveEnemyRep = "A";
    const std::string alivePenemyRep = u8"\u0416";
    const std::string aliveProtagRep = u8"\u00F6";
    const std::string deadEnemyRep= u8"\u023A";
    const std::string deadPenemyRep = "w";
    const std::string deadProtagRep =u8"\u00D8";
    const std::string tileRep = " ";
};

#endif // TERMINAL_UI_H
