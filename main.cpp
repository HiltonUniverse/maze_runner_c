#include "mainwindow.h"
#include <QApplication>
#include <iostream>
#include <memory>
#include "controller.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    Controller::Create();
    Controller * controller = Controller::getControllerInstance();
    controller->controllerSignalConnector();
    return app.exec();
}



//crushing png to get rid of iCCP error: https://stackoverflow.com/questions/22745076/libpng-warning-iccp-known-incorrect-srgb-profile
//pngcrush -ow -rem allb -reduce file.png
