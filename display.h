#ifndef DISPLAY_H
#define DISPLAY_H

#include <QFrame>
#include <QGraphicsView>
#include <QWheelEvent>
#include <QSlider>
#include <QToolButton>
#include <QSpinBox>
#include <QLabel>
#include <QProgressBar>
#include <QRadioButton>
#include <QLCDNumber>
#include <QGridLayout>

#include "config.h"
#include "view.h"

class MainWindow;
class QWidget;

class Display : public QWidget, public View
{
    Q_OBJECT
public:
    Display(QWidget *parent = 0);
    QGraphicsView & getView() const;

    void drawMazeSolverUI();
    void hideOrDisplayStartingUIWidgets(bool value);
    void hideOrDisplayMazeSolverUIWidgets(bool value);

    /*Get User Set Values*/
     QSpinBox *getSpinBoxEnemy() const;
     QSpinBox * getSpinBoxHealth() const;
     QToolButton *getStartButton() const;
     QToolButton *getRestartButton() const;
     QToolButton *getTerminalButton() const;
     QRadioButton &getManualModeButton() const;
     QRadioButton &getAutoModeButton() const;
     QRadioButton &getMazeSolverRadioButton() const;
     QToolButton &getGoToStartingUI() const;
     QToolButton &getStartPathFinding() const;
     QSpinBox &getStartXSpinBox() const;
     QSpinBox &getStartYSpinBox() const;
     QSpinBox &getEndXSpinBox() const;
     QSpinBox &getEndYSpinBox() const;
     QLCDNumber &getTotalTimeDisplayer() const{return *m_totalTimeDisplayer;}
     QProgressBar &getProtaHealth() const{return *m_protaHealth;}

signals:
     void foo(const QString& arg);

public slots:
     void zoomIn(int level = 1);
    void zoomOut(int level = 1);

private slots:
    void setupMatrix();
    void setResetButtonEnabled();
    void resetView();

private:
    //used to manually control the game
    virtual bool userAction();
    //used to draw the view
    virtual bool makeView();

    virtual bool moveView(int x, int y);

    QGraphicsScene * m_displaysScene;
    QToolButton * m_resetButton;
    QGraphicsView * m_graphicsView;
    QSlider * m_zoomSlider;
    QSlider * m_rotateSlider;

    QGridLayout *topLayout;
    /*------WIDGETS FOR PATHFINDING UI PART------*/
    QProgressBar * m_protaHealth;   //decreases when attacking enemy
    QProgressBar * m_protaEnergy;   //decreases when we travel through the tiles

    QSpinBox * m_spinBoxEnemy;
    QSpinBox * m_spinBoxHealth;

    QLabel * m_labelHealth;
    QLabel * m_labelEnemy;
    QLabel * m_labelProtaHealth;
    QLabel * m_labelprotaEnergy;
    QLabel * m_modeLabel;

    QToolButton * m_startButton;
    QToolButton * m_terminalButton;
    QToolButton * m_restartButton;
    QRadioButton * m_manualModeRadioButton;
    QRadioButton * m_autoModeRadioButton;
    QRadioButton * m_mazeSolverRadioButton;

    /*------WIDGETS FOR MAZE SOLVER UI PART------*/
    QLabel * m_startXLabel;
    QLabel * m_startYLabel;
    QSpinBox * m_startXSpinBox;
    QSpinBox * m_startYSpinBox;

    QLabel * m_endXLabel;
    QLabel * m_endYLabel;
    QSpinBox * m_endXSpinBox;
    QSpinBox * m_endYSpinBox;

    QLabel * m_totalTimeLabel;
    QLCDNumber * m_totalTimeDisplayer;
    QToolButton * m_goToStartingUI;
    QToolButton * m_startPathFinding;
};

#endif // DISPLAY_H
