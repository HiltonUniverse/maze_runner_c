#ifndef HERO_H
#define HERO_H

#include <QKeyEvent>
#include <QWidget>
#include "config.h"
#include "imagegenerator.h"

class Hero : public Protagonist , public ImageGenerator
{
public:
    Hero();
};

#endif // HERO_H
