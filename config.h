#ifndef CONFIG_H
#define CONFIG_H

#include <QString>
#include "../the_maze_runner/lib/world.h"

const QString MAZE1 = ":/assets/maze1.png";
const QString MAZE2 = ":/assets/maze2.png";
const QString MAZE3 = ":/assets/maze3.png";
const QString WORLD = ":/assets/world.png";
const QString TESTWORLD = ":/assets/testWorld.png";
const QString TESTWORLD2 = ":/assets/testWorld2.jpeg";
const QString PROTAGONIST = ":/assets/prota.png";
const QString PENEMY = ":/assets/pEnemy.png";
const QString NENEMY = ":/assets/nEnemy.png";
const QString ZOOMIN = ":/assets/zoomin.png";
const QString ZOOMOUT = ":/assets/zoomout.png";
const QString HP = ":/assets/HP.png";

/* This SCALE is used to scale EACH PIXEL of a QImage or Qpixel.*/
const int WORLDSCALE =45;   //for WORLD USE 2, for TESTWORL USE 45
const int STEPCOST = 10;    //NEED TO BE CHECKED
const int MAIN_TIMER = 300;
const int CONDITIONAL_TIMER = 300;

#endif // CONFIG_H
